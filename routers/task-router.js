const taskController = require('../controllers/task-controller');

function TaskRouter() {
    this.startRouting = app => {
        app.get('/tasks', taskController.getTasks);
        app.post('/tasks', taskController.postTask);
    };
};

module.exports = new TaskRouter();

