const chai = require('chai');
const should = chai.should();
const expect = chai.expect;

const taskModel = require('../../models/task-model');

describe('## SIMPLE TEST ## ', () => {
    it('simple unit test', () => {
        const tasks = taskModel.getTasks();
        tasks.should.be.a('array');
    });
});