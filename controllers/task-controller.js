const taskModel = require('../models/task-model.js');

exports.getTasks = async (req, res) => {
    const tasks = taskModel.getTasks();
    res.json(tasks);
};

exports.postTask = (req, res) => {
    res.json({ status: 'post task' });
};